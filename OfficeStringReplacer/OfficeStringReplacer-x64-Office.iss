﻿#define MyAppName "OfficeStringReplacer"
#define AppVersion GetFileVersion(SourcePath + "bin\x64\Release\OfficeStringReplacer.exe")
[Setup]
AppName={#MyAppName}
AppVerName={#MyAppName}
VersionInfoVersion={#AppVersion}
AppVersion={#AppVersion}
AppPublisher=ClearCode Inc.
DefaultDirName={autopf}\OfficeStringReplacer
ShowLanguageDialog=no
Compression=lzma2
SolidCompression=yes
OutputDir=dest
OutputBaseFilename=OfficeStringReplacerSetup-{#SetupSetting("AppVersion")}-x64-Office
VersionInfoDescription=OfficeStringReplacerSetup
ArchitecturesAllowed=x64
ArchitecturesInstallIn64BitMode=x64
LicenseFile=..\LICENSE.txt
PrivilegesRequired=lowest
PrivilegesRequiredOverridesAllowed=dialog

[Languages]
Name: en; MessagesFile: "compiler:Default.isl"
Name: ja; MessagesFile: "compiler:Languages\Japanese.isl"

[Files]
Source: "bin\x64\Release\*.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "bin\x64\Release\*.config"; DestDir: "{app}"; Flags: ignoreversion
Source: "bin\x64\Release\*.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Licenses\*"; DestDir: "{app}\Licenses"; Flags: ignoreversion recursesubdirs
Source: "..\LICENSE.txt"; DestDir: "{app}"; Flags: ignoreversion
Source: "{src}\DefaultConfig\config.json"; DestDir: "{autoappdata}\{#MyAppName}"; Flags: ignoreversion external skipifsourcedoesntexist

[Dirs]
Name: "{autoappdata}\{#MyAppName}"

[Icons]
Name: "{autodesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppName}.exe"; WorkingDir: "{app}" 