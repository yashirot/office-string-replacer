﻿#define MyAppName "OfficeStringReplacerEvaluation"
#define AppVersion GetFileVersion(SourcePath + "bin\x64\ReleaseForEvaluation\OfficeStringReplacerEvaluation.exe")
[Setup]
AppName={#MyAppName}
AppVerName={#MyAppName}
VersionInfoVersion={#AppVersion}
AppVersion={#AppVersion}
AppPublisher=ClearCode Inc.
DefaultDirName={autopf}\OfficeStringReplacerEvaluation
ShowLanguageDialog=no
Compression=lzma2
SolidCompression=yes
OutputDir=dest
OutputBaseFilename=OfficeStringReplacerEvaluationSetup-{#SetupSetting("AppVersion")}-x64-Office
VersionInfoDescription=OfficeStringReplacerEvaluationSetup
ArchitecturesAllowed=x64
ArchitecturesInstallIn64BitMode=x64
LicenseFile=..\LICENSE.txt
PrivilegesRequired=lowest
PrivilegesRequiredOverridesAllowed=dialog

[Languages]
Name: en; MessagesFile: "compiler:Default.isl"
Name: ja; MessagesFile: "compiler:Languages\Japanese.isl"

[Files]
Source: "bin\x64\ReleaseForEvaluation\*.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "bin\x64\ReleaseForEvaluation\*.config"; DestDir: "{app}"; Flags: ignoreversion
Source: "bin\x64\ReleaseForEvaluation\*.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Licenses\*"; DestDir: "{app}\Licenses"; Flags: ignoreversion recursesubdirs
Source: "..\LICENSE.txt"; DestDir: "{app}"; Flags: ignoreversion
Source: "{src}\DefaultConfig\config.json"; DestDir: "{autoappdata}\{#MyAppName}"; Flags: ignoreversion external skipifsourcedoesntexist

[Dirs]
Name: "{autoappdata}\{#MyAppName}"

[Icons]
Name: "{autodesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppName}.exe"; WorkingDir: "{app}" 