﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OfficeStringReplacerCore.Model;
using OfficeStringReplacerCore.Domain.Replacer;
using OfficeStringReplacerTests;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeStringReplacerCore.Domain.Searcher;

namespace OfficeStringReplacerCore.Domain.Replacer.Tests
{
    [TestClass()]
    public class ExcelReplacerTest
    {
        public TestContext TestContext { get; set; }

        public static string InFolder = @"..\..\..\TestFile\Excel\In";
        public static string OutFolder = @"..\..\..\TestFile\Excel\Out";

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            try
            {
                Directory.Delete(OutFolder, true);
            }
            catch
            {
            }
            Directory.CreateDirectory(OutFolder);
        }


        [TestMethod()]
        [TestCase("test1.xlsx", "シート2", "シート2_置換後")]
        [TestCase("test2.xlsx", @"Out/test1.xlsx", @"Out_置換後/test1.xlsx")]
        [TestCase("test3.xlsx", "支払日", "支払日_置換後")]
        [TestCase("test4.xlsx", "web_image", "ウェブイメージ_置換後")]
        [TestCase("test5.xlsm", "テスト１", "テスト１_置換後")]
        [TestCase("test6.xlsx", "スクリプト", "スクリプト_置換後")]
        [TestCase("test7.xls", "シート2", "シート2_置換後")]
        [TestCase("test8.xls", "テスト１", "テスト１_置換後")]
        [TestCase("test9.xlsm", "テスト１", "テスト１_置換後")]
        public void ReplaceTest()
        {
            TestContext.Run((string targetFile, string from, string to) =>
            {
                var originalTestfile = Path.Combine(InFolder, targetFile);
                var testFile = Path.GetFullPath(Path.Combine(OutFolder, targetFile));
                File.Copy(originalTestfile, testFile);
                var param = new ProcesserParam()
                {
                    TargetPath = testFile,
                    ValueProcesserParam = new ValueProcesserParam()
                    {
                        From = from,
                        To = to,
                        IgnoreCase = true
                    }
                };
                var context = new Context();
                var excelReplacer = new ExcelReplacer(context, param);
                excelReplacer.Process();
                context.ProcessedFiles.Contains(testFile).IsTrue(targetFile);
            });
        }
    }
}