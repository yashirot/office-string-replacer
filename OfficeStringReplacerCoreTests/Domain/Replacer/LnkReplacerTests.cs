﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OfficeStringReplacerCore.Model;
using OfficeStringReplacerCore.Domain.Replacer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeStringReplacerCore.Domain.Replacer.Tests
{
    [TestClass()]
    public class ShortcutReplacerTest
    {
        public TestContext TestContext { get; set; }

        public static string InFolder = @"..\..\..\TestFile\Lnk\In";
        public static string OutFolder = @"..\..\..\TestFile\Lnk\Out";

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            try
            {
                Directory.Delete(OutFolder, true);
            }
            catch
            {
            }
            Directory.CreateDirectory(OutFolder);
        }

        [TestMethod()]
        [TestCase("test1.lnk", "In", "Out")]
        public void ReplaceTest()
        {
            TestContext.Run((string targetFile, string from, string to) =>
            {
                var originalTestfile = Path.Combine(InFolder, targetFile);
                var testFile = Path.GetFullPath(Path.Combine(OutFolder, targetFile));
                File.Copy(originalTestfile, testFile);
                var param = new ProcesserParam()
                {
                    TargetPath = testFile,
                    ValueProcesserParam = new ValueProcesserParam()
                    {
                        From = from,
                        To = to,
                        IgnoreCase = true
                    }
                };
                var context = new Context();
                var shortcutReplacer = new ShortcutReplacer(context, param);
                shortcutReplacer.Process(testFile);
                context.ProcessedFiles.Contains(testFile).IsTrue(targetFile);
            });
        }
    }
}