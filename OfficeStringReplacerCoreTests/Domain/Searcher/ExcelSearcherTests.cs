﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OfficeStringReplacerCore.Model;
using OfficeStringReplacerCore.Domain.Searcher;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeStringReplacerCore.Domain.Replacer.Tests
{
    [TestClass()]
    public class ExcelSearcherTests
    {
        public static string InFolder = @"..\..\TestFile\Excel\In";
        public static string OutFolder = @"..\..\TestFile\Excel\Out";

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            try
            {
                Directory.Delete(OutFolder, true);
            }
            catch
            {
            }
            Directory.CreateDirectory(OutFolder);
        }

        [TestMethod()]
        public void ProcessFileTest()
        {
            var originalTestfile = Path.Combine(InFolder, "test1.xlsx");
            var param = new ProcesserParam()
            {
                TargetPath = InFolder,
                ValueProcesserParam = new ValueProcesserParam()
                {
                    From = "シート1_A1",
                }
            };
            var excelSearcher = new ExcelSearcher(new Context(), param);
            excelSearcher.Process(originalTestfile);
        }

        [TestMethod()]
        public void ProcessFolderTest()
        {
            var param = new ProcesserParam()
            {
                TargetPath = InFolder,
                ValueProcesserParam = new ValueProcesserParam()
                {
                    From = "テキスト",
                }
            };
            var excelSearcher = new ExcelSearcher(new Context(), param);
            param.ValueProcesserParam.From = "testimage";
            excelSearcher.Process();
            excelSearcher = new ExcelSearcher(new Context(), param);
            excelSearcher.Process();
        }
    }
}