@REM ==================
@REM Compile C# sources
@REM ==================
msbuild /p:Configuration=Release /p:Platform=x64 /t:Rebuild
msbuild /p:Configuration=Release /p:Platform=x86 /t:Rebuild

pushd OfficeStringReplacer
  @REM ================
  @REM Build installers
  @REM ================

  iscc.exe /O..\dest OfficeStringReplacer-x64-Office.iss
  iscc.exe /O..\dest OfficeStringReplacer-x86-Office.iss

  @REM ===============================
  @REM Build installers for evaluation
  @REM ===============================

  iscc.exe /O..\dest OfficeStringReplacerEvaluation-x64-Office.iss
  iscc.exe /O..\dest OfficeStringReplacerEvaluation-x86-Office.iss
popd

pushd ShortcutStringReplacer
  @REM ================
  @REM Build installers
  @REM ================

  iscc.exe /O..\dest ShortcutStringReplacer-x64.iss
  iscc.exe /O..\dest ShortcutStringReplacer-x86.iss
popd