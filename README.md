# office-string-replacer

## 概要

Microsoft Office製品で使用するファイル内の文字列を置換します。
Excel、Word、PowerPoint、Accessの各ファイル形式をサポートしています。
VBAマクロの文字列置換もサポートしています。

また、ショートカットファイル（.lnk）のリンク先の置換もサポートしています。

## 使用方法

### 環境

* Windows 10, 11
* Microsoft Officeがインストールされていること
* Excel、Word、PowerPointにて、VBAプロジェクト オブジェクト モデルへのアクセスを信頼していること
  * 各アプリの「オプション」 -> 「トラストセンター」 -> 「トラストセンターの設定」 -> 「マクロの設定」 -> 「開発者向けのマクロ設定」 -> 「VBAプロジェクト オブジェクト モデルへのアクセスを信頼する」にチェックを入れる 

### インストール

64bitのOfficeを使用している場合、`OfficeStringReplacerSetup-{version}-x64-Office.exe`をインストールします。
32bitのOfficeを使用している場合、`OfficeStringReplacerSetup-{version}-x86-Office.exe`をインストールします。

Windowsのアーキテクチャではなく、Officeのアーキテクチャであることに注意してください。

セットアップと同じフォルダーに「DefaultConfig\config.json」が存在する場合、その設定がインストールされます。
設定ファイルは`%APPDATA%\OfficeStringReplacer\config.json`にインストールされます。

### 使用方法

* 「検索対象パス」に検索対象のフォルダーまたはファイルを入力します
* 「バックアップフォルダー」に置換時にファイルをバックアップするフォルダーを指定します
  * ここで指定したフォルダー配下に`osr.backup.{変換日時}`というフォルダーを作成し、さらにその配下に元のファイルがバックアップされます
  * ルートパス（ドライブやファイルサーバ名）からのディレクトリ構造ごとバックアップされます
    * 例えば、バックアップフォルダーが`D:\backup`で、置換対象のファイルが`C:\testfolder\testfile.lnk`の場合、`D:\backup\osr.backup.{変換日時}\testfolder\testfile.lnk`にバックアップされます
* 「対象ファイル」から対象となるファイルの形式を指定します
* 「置換前文字列」に置換前の文字列を入力します
* 「置換後文字列」に置換後の文字列を入力します
* 「対象ファイルを検索」ボタンを押下します
  * 「対象ファイル」セレクトボックスに、「置換前文字列」を含むファイル一覧が出力されます
  * フォルダーは再帰的に検索されます
* 「選択したファイルを置換する」ボタンを押下すると、「対象ファイル」セレクトボックスで選択されているファイルを置換します
  * 「置換前文字列」が「置換後文字列」に変換されます

### 設定ファイル

設定ファイルは`%APPDATA%\OfficeStringReplacer\config.json`に存在します。
設定はJSON形式で指定します。

```
{
    "From": null,
    "To": null,
    "IgnoreCase": false,
    "XmlTagsToBeIgnored": [
        "mc:AlternateContent"
    ],
    "BackupDirectory": null
}
```

* `From`
  * 起動時のデフォルトの「置換前文字列」を指定します。
  * 例: `"From" : "foo"`
  * デフォルト値: `null`
* `To`
  * 起動時のデフォルトの「置換後文字列」を指定します。
  * 例: `"To" : "bar"`
  * デフォルト値: `null`
* `IgnoreCase`
  * 起動時のデフォルトの「大文字小文字を無視する」を指定します。
  * 例: `"IgnoreCase" : true`
  * デフォルト値: `false`
* `XmlTagsToBeIgnored`
  * OfficeファイルをOpen XMLとして展開したとき、検索、置換から無視するXMLのタグ名をリスト形式で指定します。
  * 例: 
    ```
    "XmlTagsToBeIgnored": [
        "mc:AlternateContent",
        "workbook"
    ]
    ```
  * デフォルト値: 
    ```    
    "XmlTagsToBeIgnored": [
        "mc:AlternateContent"
    ]
    ```
* `BackupDirectory`
  * 起動時のデフォルトの「バックアップフォルダー」を指定します。
  * 例: `"BackupDirectory" : "D:\"`
  * デフォルト値: `null`

### サポート対象ファイル形式

* Excel
  * `.xlsx`
  * `.xlsm`
  * `.xls`
* Word
  * `.docx`
  * `.docm`
  * `.doc`
* PowerPoint
  * `.pptx`
  * `.pptm`
  * `.ppt`
* Access
  * `.accdb`
  * `.mdb`
* ショートカット
  * `.lnk`

### 制限事項

#### 全般

* パスワードロックされたOffice製品はサポート対象外です
* 書き込み権限がないファイルはサポート対象外です

#### Access

* Accessマクロはサポート対象外です
  * ※VBAマクロはサポート対象です
