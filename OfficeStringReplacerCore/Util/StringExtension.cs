﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace OfficeStringReplacerCore.Util
{
    public static class StringExtension
    {


        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return source?.IndexOf(toCheck, comp) >= 0;
        }

        public static bool ContainsWithRegex(this string source, string toCheck, RegexOptions options)
        {
            return Regex.IsMatch(source, toCheck, options);
        }

        public static string ReplaceWithRegex(this string source, string from, string to, RegexOptions options)
        {
            return Regex.Replace(source, from, to, options);
        }

        public static string EscapeToAccessLike(this string source)
        {
            var chars = source.ToCharArray();
            var length = chars.Length;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < length; i++)
            {
                char ch = chars[i];
                switch (ch)
                {
                    case '*':
                    case '?':
                    case '#':
                    case '-':
                        sb.Append($"[{ch}]");
                        break;
                    case '[':
                        if (i == length - 1 &&
                            chars[i + 1] == ']')
                        {
                            sb.Append($"[[]]");
                            i++;
                            break;

                        }
                        sb.Append($"[{ch}]");
                        break;
                    case '\'':
                        sb.Append("''");
                        break;
                    default:
                        sb.Append(ch);
                        break;
                }
            }
            return sb.ToString();
        }
    }
}
