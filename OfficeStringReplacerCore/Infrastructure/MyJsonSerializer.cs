﻿using System.IO;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace OfficeStringReplacerCore.Infrastructure
{
    public static class MyJsonSerializer<T>
    {
        private static readonly JsonSerializerOptions _options = new JsonSerializerOptions
        {
            Converters = { new JsonStringEnumConverter(JsonNamingPolicy.CamelCase) },
            PropertyNameCaseInsensitive = true,
        };

        public static T Deserialize(string jsonString)
        {
            return JsonSerializer.Deserialize<T>(jsonString, _options);
        }

        public static T LoadFromFile(string filePath)
        {
            if (!File.Exists(filePath))
            {
                return default;
            }
            var jsonString = File.ReadAllText(filePath, Encoding.UTF8);
            return Deserialize(jsonString);
        }

        public static void SaveAsFile(T obj, string filePath)
        {
            string jsonString = JsonSerializer.Serialize(obj, _options);
            string tmpfile = Path.GetTempFileName();
            File.WriteAllText(tmpfile, jsonString);
            string directory = Path.GetDirectoryName(filePath);
            Directory.CreateDirectory(directory);
            File.Move(tmpfile, filePath);
        }
    }
}
