﻿using OfficeStringReplacerCore.Model;
using OfficeStringReplacerCore.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeStringReplacerCore.Infrastructure
{
    public class ConfigLoader
    {
        public static Config Load()
        {
            var configPath = Utils.GetUserConfigFilePath();
            if (File.Exists(configPath))
            {
                return MyJsonSerializer<Config>.LoadFromFile(configPath);
            }
            configPath = Utils.GetCommonConfigFilePath();
            if (File.Exists(configPath))
            {
                return MyJsonSerializer<Config>.LoadFromFile(configPath);
            }
            return new Config();
        }
    }
}
