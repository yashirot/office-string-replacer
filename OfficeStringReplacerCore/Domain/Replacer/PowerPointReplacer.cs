﻿using Ionic.Zip;
using Microsoft.Office.Interop.PowerPoint;
using OfficeStringReplacerCore.Domain.Processer;
using OfficeStringReplacerCore.Model;
using OfficeStringReplacerCore.Util;
using System.IO;

namespace OfficeStringReplacerCore.Domain.Replacer
{
    public class PowerPointReplacer : PowerPointProcesser
    {
        public PowerPointReplacer(Context context, ProcesserParam param) : base(context, param)
        {
            XmlFileProcesser = new XmlFileReplacer(context, param);
            VbaScriptProcesser = new PowerPointVbaScriptReplacer(context, param);
        }

        private void CompressAsPowerPointFile(string sourceFolderPath, string destFilePath)
        {
            var tmpFilePath = Utils.CreateUniqTempFilePath();
            try
            {
                using (ZipFile zipFileForDestFilePath = new ZipFile())
                {
                    zipFileForDestFilePath.AddDirectory(sourceFolderPath, "");
                    zipFileForDestFilePath.Save(tmpFilePath);
                }
                var ext = Path.GetExtension(destFilePath);
                switch (ext)
                {
                    case ".ppt":
                        using (var powerpointApp = new DisposableCom<Microsoft.Office.Interop.PowerPoint.Application>(new Microsoft.Office.Interop.PowerPoint.Application()))
                        using (var presentations = new DisposableCom<Presentations>(powerpointApp.Value.Presentations))
                        using (var presentation = new DisposableCom<Presentation>(presentations.Value.Open(tmpFilePath)))
                        {
                            try
                            {
                                if (File.Exists(destFilePath))
                                {
                                    File.Delete(destFilePath);
                                }
                                presentation.Value.SaveAs(destFilePath, PpSaveAsFileType.ppSaveAsPresentation);
                            }
                            finally
                            {
                                presentation.Value.Close();
                                powerpointApp.Value.Quit();
                            }
                        }
                        break;
                    default:
                        if (File.Exists(destFilePath))
                        {
                            File.Delete(destFilePath);
                        }
                        File.Move(tmpFilePath, destFilePath);
                        break;
                }
            }
            finally
            {
                if (File.Exists(tmpFilePath))
                {
                    File.Delete(tmpFilePath);
                }
            }
        }

        protected override void PostProcess(string sourceFolderPath, string destFilePath)
        {
            if (Context.Status == ContextStatus.Fail)
            {
                return;
            }
            CompressAsPowerPointFile(sourceFolderPath, destFilePath);
        }
    }
}
