﻿using OfficeStringReplacerCore.Domain.Processer;
using OfficeStringReplacerCore.Model;

namespace OfficeStringReplacerCore.Domain.Replacer
{
    public class AccessVbaScriptReplacer : AccessVbaScriptProcesser
    {
        public AccessVbaScriptReplacer(Context context, ProcesserParam param) : base(context, param)
        {
            ValueProcesser = new ValueReplacer(param.ValueProcesserParam);
        }
    }
}
