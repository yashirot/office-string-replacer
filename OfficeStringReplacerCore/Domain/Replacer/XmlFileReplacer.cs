﻿using OfficeStringReplacerCore.Domain.Processer;
using OfficeStringReplacerCore.Model;
using System.IO;
using System.Xml;

namespace OfficeStringReplacerCore.Domain.Replacer
{
    public class XmlFileReplacer: XmlFileProcesser
    {
        public XmlFileReplacer(Context context, ProcesserParam param) : base(context, param)
        {
            ValueProcesser = new ValueReplacer(param.ValueProcesserParam);
        }

        protected override void PostProcess(XmlDocument document, string destFilePath)
        {
            if (File.Exists(destFilePath))
            {
                File.Delete(destFilePath);
            }
            
            document.Save(destFilePath);
        }
    }
}
