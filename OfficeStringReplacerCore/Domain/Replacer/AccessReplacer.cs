﻿using OfficeStringReplacerCore.Domain.Processer;
using OfficeStringReplacerCore.Model;
using System.IO;

namespace OfficeStringReplacerCore.Domain.Replacer
{
    public class AccessReplacer : AccessProcesser
    {
        public AccessReplacer(Context context, ProcesserParam param) : base(context, param)
        {
            ValueProcesser = new ValueReplacer(param.ValueProcesserParam);
            VbaScriptProcesser = new AccessVbaScriptReplacer(context, param);
        }

        protected override void PreProcess(string sourceFolderPath)
        {
        }

        protected override void PostProcess(string sourceFolderPath, string destFilePath)
        {
            if (File.Exists(destFilePath))
            {
                File.Delete(destFilePath);
            }
            File.Move(sourceFolderPath, destFilePath);
        }
    }
}
