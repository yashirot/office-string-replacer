﻿using Ionic.Zip;
using Microsoft.Office.Interop.Excel;
using OfficeStringReplacerCore.Domain.Processer;
using OfficeStringReplacerCore.Model;
using OfficeStringReplacerCore.Util;
using System.IO;
using System.Runtime.InteropServices;

namespace OfficeStringReplacerCore.Domain.Replacer
{
    public class ExcelReplacer : ExcelProcesser
    {
        public ExcelReplacer(Context context, ProcesserParam param) : base(context, param)
        {
            XmlFileProcesser = new XmlFileReplacer(context, param);
            VbaScriptProcesser = new ExcelVbaScriptReplacer(context, param);
        }

        private void CompressAsExcelFile(string sourceFolderPath, string destFilePath)
        {
            var tmpFilePath = Utils.CreateUniqTempFilePath();
            try
            {
                using (ZipFile zipFileForDestFilePath = new ZipFile())
                {
                    zipFileForDestFilePath.AddDirectory(sourceFolderPath, "");
                    zipFileForDestFilePath.Save(tmpFilePath);
                }
                var ext = Path.GetExtension(destFilePath);
                switch (ext)
                {
                    case ".xls":
                        using (var excelApp = new DisposableCom<Application>(new Application()
                        {
                            EnableEvents = false,
                            DisplayAlerts = false,
                        },
                        (_) => { _.Quit(); }))
                        using (var workbooks = new DisposableCom<Workbooks>(excelApp.Value.Workbooks))
                        using (var workbook = new DisposableCom<Workbook>(
                            workbooks.Value.Open(tmpFilePath, IgnoreReadOnlyRecommended: true),
                            (_) => { _.Close(false); }))
                        {
                            if (File.Exists(destFilePath))
                            {
                                File.Delete(destFilePath);
                            }
                            workbook.Value.CheckCompatibility = false;
                            workbook.Value.DoNotPromptForConvert = true;
                            workbook.Value.SaveAs(destFilePath, XlFileFormat.xlExcel8);
                        }
                        break;
                    default:
                        if (File.Exists(destFilePath))
                        {
                            File.Delete(destFilePath);
                        }
                        File.Move(tmpFilePath, destFilePath);
                        break;
                }
            }
            finally
            {
                if (File.Exists(tmpFilePath))
                {
                    File.Delete(tmpFilePath);
                }
            }
        }

        protected override void PostProcess(string sourceFolderPath, string destFilePath)
        {
            if (Context.Status == ContextStatus.Fail)
            {
                return;
            }
            CompressAsExcelFile(sourceFolderPath, destFilePath);
        }
    }
}
