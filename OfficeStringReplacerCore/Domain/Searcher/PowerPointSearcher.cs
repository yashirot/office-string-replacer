﻿using OfficeStringReplacerCore.Domain.Processer;
using OfficeStringReplacerCore.Model;

namespace OfficeStringReplacerCore.Domain.Searcher
{
    public class PowerPointSearcher : PowerPointProcesser
    {
        public PowerPointSearcher(Context context, ProcesserParam param) : base(context, param)
        {
            XmlFileProcesser = new XmlFileSearcher(context, param);
            VbaScriptProcesser = new PowerPointVbaScriptSearcher(context, param);
        }
    }
}
