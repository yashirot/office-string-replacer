﻿using OfficeStringReplacerCore.Domain.Processer;
using OfficeStringReplacerCore.Model;

namespace OfficeStringReplacerCore.Domain.Searcher
{
    public class ShortcutSearcher : ShortcutProcesser
    {
        public ShortcutSearcher(Context context, ProcesserParam param) : base(context, param)
        {
            ValueProcesser = new ValueSearcher(param.ValueProcesserParam);
        }

        protected override void PreProcess(string sourceFolderPath)
        {
        }

        protected override void PostProcess(string sourceFolderPath, string destFilePath)
        {
        }
    }
}