﻿using Microsoft.Office.Interop.PowerPoint;
using Microsoft.Vbe.Interop;
using OfficeStringReplacerCore.Model;
using System.IO;

namespace OfficeStringReplacerCore.Domain.Processer
{
    public class PowerPointVbaScriptProcesser : VbaScriptProcesser
    {
        public PowerPointVbaScriptProcesser(Context context, ProcesserParam param) : base(context, param)
        { }

        public override void Process(string file)
        {
            var fullPath = Path.GetFullPath(file);
            using (var powerpointApp = new DisposableCom<Microsoft.Office.Interop.PowerPoint.Application>(
                new Microsoft.Office.Interop.PowerPoint.Application(),
                (_) => { _.Quit(); }))
            using (var presentations = new DisposableCom<Presentations>(powerpointApp.Value.Presentations))
            using (var presentation = new DisposableCom<Presentation>(
                presentations.Value.Open(fullPath),
                (_) =>
                {
                    _.Save();
                    _.Close();
                }))
            using (var vbe = new DisposableCom<VBE>(powerpointApp.Value.VBE))
            using (var projects = new DisposableCom<VBProjects>(vbe.Value.VBProjects))
            {
                Process(projects.Value);
            }
        }
    }
}
