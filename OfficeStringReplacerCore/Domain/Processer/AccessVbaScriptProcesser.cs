﻿using Microsoft.Vbe.Interop;
using OfficeStringReplacerCore.Model;
using System.IO;

namespace OfficeStringReplacerCore.Domain.Processer
{
    public class AccessVbaScriptProcesser : VbaScriptProcesser
    {
        public AccessVbaScriptProcesser(Context context, ProcesserParam param) : base(context, param)
        {
        }

        public override void Process(string file)
        {
            using (var accessApp = new DisposableCom<Microsoft.Office.Interop.Access.Application>(
                new Microsoft.Office.Interop.Access.Application(),
                (_) => { _.Quit(Microsoft.Office.Interop.Access.AcQuitOption.acQuitSaveAll); }))
            {
                accessApp.Value.OpenCurrentDatabase(Path.GetFullPath(file));
                try
                {
                    Process(accessApp.Value);
                }
                finally
                {
                    accessApp.Value.CloseCurrentDatabase();
                }
            }
        }

        public void Process(Microsoft.Office.Interop.Access.Application accessApp)
        {
            using (var vbe = new DisposableCom<VBE>(accessApp.VBE))
            using (var projects = new DisposableCom<VBProjects>(vbe.Value.VBProjects))
            {
                Process(projects.Value);
            }
        }
    }
}
