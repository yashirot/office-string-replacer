﻿using Microsoft.Office.Interop.Access;
using Microsoft.Office.Interop.Access.Dao;
using OfficeStringReplacerCore.Infrastructure;
using OfficeStringReplacerCore.Model;
using OfficeStringReplacerCore.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace OfficeStringReplacerCore.Domain.Processer
{
    public abstract class AccessProcesser : Processer
    {
        protected AccessVbaScriptProcesser VbaScriptProcesser { get; set; }

        public AccessProcesser(Context context, ProcesserParam param) : base(context, param)
        {
        }

        public bool IsTarget(string path)
        {
            var ext = Path.GetExtension(path);
            switch (ext)
            {
                case ".mdb":
                case ".accdb":
                    return true;
                default:
                    return false;
            }
        }

        private void ProcessTables(Database db)
        {
            Logger.Debug($"Start ProcessTables");
            try
            {
                ValueProcesser.ProcessTables(Context, db);
            }
            finally
            {
                Logger.Debug($"Finish ProcessTables");
            }
        }

        private void ProcessQueries(Database db)
        {
            Logger.Debug($"Start ProcessQueries");
            try
            {
                using (var queryDefs = new DisposableCom<QueryDefs>(db.QueryDefs))
                {
                    foreach (var queryDef in Utils.EnamerateAsDisposableCom<QueryDef>(queryDefs.Value))
                    {
                        using (queryDef)
                        {
                            string name = queryDef.Value.Name;
                            Logger.Debug($"Query name: {name}");
                            ValueProcesser.Process(Context, queryDef.Value, nameof(queryDef.Value.Name));
                            if (Context.SkipRest)
                            {
                                return;
                            }
                            ValueProcesser.Process(Context, queryDef.Value, nameof(queryDef.Value.SQL));
                            if (Context.SkipRest)
                            {
                                return;
                            }
                        }
                    }
                }
            }
            finally
            {
                Logger.Debug($"Finish ProcessQueries");
            }
        }

        private void ProcessControls(IEnumerable controls)
        {
            Logger.Debug($"Start ProcessControls");
            try
            {
                foreach (var control in Utils.EnamerateAsDisposableCom<object>(controls))
                {
                    using (control)
                    {
                        var type = control.Value.GetType();
                        var properties = type.GetProperties();
                        foreach (var property in properties)
                        {
                            if (!(property.CanRead && property.CanWrite))
                            {
                                continue;
                            }
                            if (property.Name == "EventProcPrefix")
                            {
                                //EventProcPrefix is readonly in view
                                //https://support.microsoft.com/ja-jp/office/eventprocprefix-%E3%83%97%E3%83%AD%E3%83%91%E3%83%86%E3%82%A3-04f4f2ea-e5e1-428a-85e2-14cf7c6af63f
                                continue;
                            }
                            if (property.PropertyType != typeof(string))
                            {
                                continue;
                            }
                            MethodInfo[] methInfos = property.GetAccessors(false);
                            if (methInfos.Length != 2)
                            {
                                continue;
                            }
                            MethodInfo setter = null;
                            MethodInfo getter = null;
                            foreach (MethodInfo methodInfo in methInfos)
                            {
                                if (methodInfo.ReturnType == typeof(void))
                                {
                                    setter = methodInfo;
                                }
                                else
                                {
                                    getter = methodInfo;
                                }
                            }
                            if (getter.GetParameters().Length != 0 || setter.GetParameters().Length != 1)
                            {
                                continue;
                            }
                            //Strictly, we should check if a property should be change or not.
                            try
                            {
                                ValueProcesser.Process(Context, control.Value, property.Name);
                                if (Context.SkipRest)
                                {
                                    return;
                                }
                            }
                            catch (Exception ex)
                            {
                                //Some properties faild to process as normal,
                                //so only debug logging.
                                Logger.Debug(ex);
                            }
                        }
                    }
                }
            }
            finally
            {
                Logger.Debug($"Finish ProcessControls");
            }
        }

        private void ProcessForm(Application accessApp, string formName)
        {
            Logger.Debug($"Start ProcessForm: {formName}");
            try
            {
                using (var forms = new DisposableCom<Forms>(accessApp.Forms))
                {
                    foreach (var form in Utils.EnamerateAsDisposableCom<Form>(forms.Value))
                    {
                        using (form)
                        {
                            if (form.Value.Name != formName)
                            {
                                continue;
                            }
                            using (var controls = new DisposableCom<Controls>(form.Value.Controls))
                            {
                                ProcessControls(controls.Value);
                            }
                            break;
                        }
                    }
                }
            }
            finally
            {
                Logger.Debug($"Finish ProcessForm");
            }
        }

        private void ProcessForms(Application accessApp)
        {
            Logger.Debug($"Start ProcessForms");
            try
            {
                using (var accessProject = new DisposableCom<CurrentProject>(accessApp.CurrentProject as CurrentProject))
                using (var doCmd = new DisposableCom<DoCmd>(accessApp.DoCmd))
                {
                    var formNameList = new List<string>();
                    using (var allForms = new DisposableCom<AllObjects>(accessProject.Value.AllForms))
                    {
                        foreach (var form in Utils.EnamerateAsDisposableCom<AccessObject>(allForms.Value))
                        {
                            using (form)
                            {
                                formNameList.Add(form.Value.Name);
                            }
                        }
                    }
                    foreach (string formName in formNameList)
                    {
                        //Some parameters like "Name" can be changed only when desgin view.
                        doCmd.Value.OpenForm(formName, AcFormView.acDesign);
                        try
                        {
                            ProcessForm(accessApp, formName);
                            if (Context.SkipRest)
                            {
                                return;
                            }
                        }
                        finally
                        {
                            doCmd.Value.Close(AcObjectType.acForm, formName);
                        }

                        //Some parameters can't be changed when design view mode,
                        //so reopen it as normal view and exec replacement again.
                        doCmd.Value.OpenForm(formName, AcFormView.acNormal);
                        try
                        {
                            ProcessForm(accessApp, formName);
                            if (Context.SkipRest)
                            {
                                return;
                            }
                        }
                        finally
                        {
                            doCmd.Value.Close(AcObjectType.acForm, formName);
                        }

                        ValueProcesser.Process(Context, doCmd.Value, AcObjectType.acReport, formName);
                        if (Context.SkipRest)
                        {
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            finally
            {
                Logger.Debug($"Finish ProcessForms");
            }
        }

        private void ProcessReport(Application accessApp, string reportName)
        {
            Logger.Debug($"Start ProcessReport: {reportName}");
            try
            {
                using (var reports = new DisposableCom<Reports>(accessApp.Reports))
                {
                    foreach (var report in Utils.EnamerateAsDisposableCom<Report>(reports.Value))
                    {
                        using (report)
                        {
                            if (report.Value.Name != reportName)
                            {
                                continue;
                            }
                            using (var controls = new DisposableCom<Controls>(report.Value.Controls))
                            {
                                ProcessControls(controls.Value);
                            }
                            break;
                        }
                    }
                }
            }
            finally
            {
                Logger.Debug($"Finish ProcessForm");
            }
        }

        private void ProcessReports(Application accessApp)
        {
            Logger.Debug($"Start ProcessForms");
            try
            {
                using (var accessProject = new DisposableCom<CurrentProject>(accessApp.CurrentProject as CurrentProject))
                using (var doCmd = new DisposableCom<DoCmd>(accessApp.DoCmd))
                {
                    var reportNameList = new List<string>();
                    using (var allReports = new DisposableCom<AllObjects>(accessProject.Value.AllReports))
                    {
                        foreach (var report in Utils.EnamerateAsDisposableCom<AccessObject>(allReports.Value))
                        {
                            using (report)
                            {
                                reportNameList.Add(report.Value.Name);
                            }
                        }
                    }
                    foreach (string reportName in reportNameList)
                    {
                        //Some parameters like "Name" can be changed only when desgin view.
                        doCmd.Value.OpenReport(reportName, AcView.acViewDesign);
                        try
                        {
                            ProcessReport(accessApp, reportName);
                            if (Context.SkipRest)
                            {
                                return;
                            }
                        }
                        finally
                        {
                            doCmd.Value.Close(AcObjectType.acReport, reportName, AcCloseSave.acSaveYes);
                        }

                        ValueProcesser.Process(Context, doCmd.Value, AcObjectType.acReport, reportName);
                        if (Context.SkipRest)
                        {
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            finally
            {
                Logger.Debug($"Finish ProcessForms");
            }
        }

        public void Process()
        {
            Context.Status = ContextStatus.Excecuting;
            var fullPath = ProcesserParam.TargetFullPath;
            try
            {
                if (ProcesserParam.ExcludeFilePathPatternList.Any(_ => Regex.IsMatch(fullPath, _, RegexOptions.IgnoreCase)))
                {
                    Context.Status = ContextStatus.Success;
                    return;
                }

                if (Directory.Exists(fullPath))
                {
                    var files = Utils.GetFiles(fullPath, @"\.(accdb|mdb)$");
                    foreach (var file in files)
                    {
                        Process(file);
                        ThrowIfCancelRequested();
                        //Search all files
                        Context.Status = ContextStatus.Excecuting;
                        Context.SubStatus = ContextSubStatus.NotProcessed;
                    }
                }
                else if (File.Exists(fullPath))
                {
                    Process(fullPath);
                }
                if (Context.FailedFiles.Any())
                {
                    Context.Status = ContextStatus.Fail;
                }
                else
                {
                    Context.Status = ContextStatus.Success;
                }
            }
            catch (CancelException ex)
            {
                Context.Status = ContextStatus.Canceled;
                Logger.Info($"Canceled: {ex}");
                throw;
            }
        }

        public void Process(string filePath)
        {
            Context.Status = ContextStatus.Excecuting;
            var fullPath = Path.GetFullPath(filePath);
            var ext = Path.GetExtension(fullPath);
            var tmpFilePath = Utils.CreateUniqTempFilePath() + ext;
            if (!IsTarget(filePath))
            {
                Context.Status = ContextStatus.Success;
                return;
            }
            var fileName = Path.GetFileName(filePath);
            if (fileName.StartsWith("~$"))
            {
                //Temporary file
                Context.Status = ContextStatus.Success;
                Context.AddSuccessFiles(filePath);
                return;
            }
            if (ProcesserParam.ExcludeFilePathPatternList.Any(_ => Regex.IsMatch(filePath, _, RegexOptions.IgnoreCase)))
            {
                Context.Status = ContextStatus.Success;
                return;
            }
            try
            {
                Logger.Info($"Start processing:{fullPath}");
                Context.ProcessingFile = fullPath;
                File.Copy(fullPath, tmpFilePath);
                PreProcess(tmpFilePath);
                using (var dao = new DisposableCom<DBEngine>(new DBEngine()))
                using (var db = new DisposableCom<Database>(dao.Value.OpenDatabase(tmpFilePath)))
                {
                    try
                    {
                        ProcessTables(db.Value);
                        if (Context.SkipRest)
                        {
                            return;
                        }
                        ProcessQueries(db.Value);
                        if (Context.SkipRest)
                        {
                            return;
                        }
                    }
                    finally
                    {
                        db.Value.Close();
                    }
                }
                using (var accessApp = new DisposableCom<Application>(
                    new Application(),
                    (_) => { _.Quit(AcQuitOption.acQuitSaveAll); }))
                {
                    accessApp.Value.OpenCurrentDatabase(Path.GetFullPath(tmpFilePath));
                    try
                    {
                        ProcessForms(accessApp.Value);
                        if (Context.SkipRest)
                        {
                            return;
                        }
                        ProcessReports(accessApp.Value);
                        if (Context.SkipRest)
                        {
                            return;
                        }
                        VbaScriptProcesser.Process(accessApp.Value);
                        if (Context.SkipRest)
                        {
                            return;
                        }
                    }
                    finally
                    {
                        accessApp.Value.CloseCurrentDatabase();
                    }
                }
                PostProcess(tmpFilePath, fullPath);
                Context.Status = ContextStatus.Success;
            }
            catch (CancelException ex)
            {
                Context.Status = ContextStatus.Canceled;
                Logger.Info($"Canceled: {ex}");
                throw;
            }
            catch (Exception ex)
            {
                Context.Status = ContextStatus.Fail;
                Logger.Error($"{ex}");
            }
            finally
            {
                switch (Context.Status)
                {
                    case ContextStatus.Success:
                        Context.AddSuccessFiles(fullPath);
                        switch (Context.SubStatus)
                        {
                            case ContextSubStatus.Processed:
                                Context.AddProcessedFiles(fullPath);
                                break;
                        }
                        break;
                    case ContextStatus.Fail:
                        Context.AddFailedFiles(fullPath);
                        break;
                }
                if (!string.IsNullOrEmpty(tmpFilePath) && File.Exists(tmpFilePath))
                {
                    File.Delete(tmpFilePath);
                }
                Logger.Info($"Finish processing:{fullPath}");
            }
        }

        protected virtual void PreProcess(string sourceFolderPath)
        {
        }

        protected virtual void PostProcess(string sourceFolderPath, string destFilePath)
        {
        }
    }
}
