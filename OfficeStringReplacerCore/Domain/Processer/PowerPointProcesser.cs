﻿using Microsoft.Office.Interop.PowerPoint;
using Microsoft.Vbe.Interop;
using OfficeStringReplacerCore.Model;
using OfficeStringReplacerCore.Util;
using System.IO;

namespace OfficeStringReplacerCore.Domain.Processer
{
    public abstract class PowerPointProcesser : XmlOfficeProcesser
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="from">Replace from</param>
        /// <param name="to">Replace to</param>
        public PowerPointProcesser(Context context, ProcesserParam param) : base(context, param)
        {
        }

        public override bool IsTarget(string path)
        {
            var ext = Path.GetExtension(path);
            switch (ext)
            {
                case ".pptx":
                case ".pptm":
                case ".ppt":
                    return true;
                default:
                    return false;
            }
        }

        protected override bool IsMacroEnabled(string path)
        {
            var ext = Path.GetExtension(path);
            switch (ext)
            {
                case ".ppt":
                    using (var powerpointApp = new DisposableCom<Microsoft.Office.Interop.PowerPoint.Application>(
                        new Microsoft.Office.Interop.PowerPoint.Application(),
                        (_) => { _.Quit(); }))
                    using (var presentations = new DisposableCom<Presentations>(powerpointApp.Value.Presentations))
                    using (var presentation = new DisposableCom<Presentation>(
                        presentations.Value.Open(path),
                        (_) => { _.Close(); }))
                    {
                        return presentation.Value.HasVBProject;
                    }
                case ".pptm":
                    return true;
                default:
                    return false;
            }
        }

        protected override string CopyAsTempOpenXMLFile(string sourceFilePath, bool isMacroEnabled)
        {
            var format = isMacroEnabled ?
                PpSaveAsFileType.ppSaveAsOpenXMLPresentationMacroEnabled :
                PpSaveAsFileType.ppSaveAsOpenXMLPresentation;
            var tmpFilePath = isMacroEnabled ?
                Utils.CreateUniqTempFilePath() + ".pptm" :
                Utils.CreateUniqTempFilePath() + ".pptx";
            using (var powerpointApp = new DisposableCom<Microsoft.Office.Interop.PowerPoint.Application>(
                new Microsoft.Office.Interop.PowerPoint.Application(),
                (_) => { _.Quit(); }))
            using (var presentations = new DisposableCom<Presentations>(powerpointApp.Value.Presentations))
            using (var presentation = new DisposableCom<Presentation>(
                presentations.Value.Open(sourceFilePath),
                (_) => { _.Close(); }))
            {
                // We can't simply copy pptx/pptm files.
                // workbook.xml's AlternateContent contains the last saved path.
                // In order to avoid matching to that just last saved path when searching,
                // we ignore AlternateContent by default.
                // On the other hand, some of paths in xml are written as relative path from
                // that path.
                // So, save it to the temporary dest file with Office's "SaveAs" method in order
                // to update the last saved path to the temporary dest path.
                // When saving, the paths written as relative paths are updated to absolete path
                // with the original last saved path.
                presentation.Value.SaveAs(tmpFilePath, format);
            }
            return tmpFilePath;
        }
    }
}
