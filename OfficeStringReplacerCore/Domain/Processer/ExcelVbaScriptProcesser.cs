﻿using Microsoft.Office.Interop.Excel;
using Microsoft.Vbe.Interop;
using OfficeStringReplacerCore.Model;
using System.IO;

namespace OfficeStringReplacerCore.Domain.Processer
{
    public abstract class ExcelVbaScriptProcesser : VbaScriptProcesser
    {
        public ExcelVbaScriptProcesser(Context context, ProcesserParam param) : base(context, param)
        { }

        public override void Process(string file)
        {
            var fullPath = Path.GetFullPath(file);
            using (var excelApp = new DisposableCom<Microsoft.Office.Interop.Excel.Application>(
                new Microsoft.Office.Interop.Excel.Application()
                {
                    EnableEvents = false
                },
                (_) => { _.Quit(); }))
            using (var workbooks = new DisposableCom<Workbooks>(excelApp.Value.Workbooks))
            using (var workbook = new DisposableCom<Workbook>(
                workbooks.Value.Open(fullPath, IgnoreReadOnlyRecommended: true),
               (_) =>
               {
                   _.Save();
                   _.Close(true);
               }))
            using (var vbe = new DisposableCom<VBE>(excelApp.Value.VBE))
            using (var projects = new DisposableCom<VBProjects>(vbe.Value.VBProjects))
            {
                Process(projects.Value);
            }
        }
    }
}
