﻿using Microsoft.Office.Interop.Word;
using Microsoft.Vbe.Interop;
using OfficeStringReplacerCore.Model;
using OfficeStringReplacerCore.Util;
using System.IO;

namespace OfficeStringReplacerCore.Domain.Processer
{
    public abstract class WordProcesser : XmlOfficeProcesser
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="from">Replace from</param>
        /// <param name="to">Replace to</param>
        public WordProcesser(Context context, ProcesserParam param) : base(context, param)
        {
        }

        public override bool IsTarget(string path)
        {
            var ext = Path.GetExtension(path);
            switch (ext)
            {
                case ".docx":
                case ".docm":
                case ".doc":
                    return true;
                default:
                    return false;
            }
        }

        protected override bool IsMacroEnabled(string path)
        {
            var ext = Path.GetExtension(path);
            switch (ext)
            {
                case ".doc":
                    using (var wordApp = new DisposableCom<Microsoft.Office.Interop.Word.Application>(
                        new Microsoft.Office.Interop.Word.Application(),
                        (_) => { _.Quit(); }))
                    using (var documents = new DisposableCom<Documents>(wordApp.Value.Documents))
                    using (var document = new DisposableCom<Document>(
                        documents.Value.Open(path),
                        (_) => { _.Close(false); }))
                    {
                        return document.Value.HasVBProject;
                    }
                case ".docm":
                    return true;
                default:
                    return false;
            }
        }

        protected override string CopyAsTempOpenXMLFile(string sourceFilePath, bool isMacroEnabled)
        {
            var format = isMacroEnabled ?
                WdSaveFormat.wdFormatXMLDocumentMacroEnabled :
                WdSaveFormat.wdFormatXMLDocument;
            var tmpFilePath = isMacroEnabled ?
                Utils.CreateUniqTempFilePath() + ".docm" :
                Utils.CreateUniqTempFilePath() + ".docx";
            using (var wordApp = new DisposableCom<Microsoft.Office.Interop.Word.Application>(
                new Microsoft.Office.Interop.Word.Application()
                {
                    DisplayAlerts = WdAlertLevel.wdAlertsMessageBox,
                },
                (_) => { _.Quit(); }))
            using (var documents = new DisposableCom<Documents>(wordApp.Value.Documents))
            using (var document = new DisposableCom<Document>(
                documents.Value.Open(sourceFilePath),
                (_) => { _.Close(false); }))
            {
                // We can't simply copy docx/docm files.
                // workbook.xml's AlternateContent contains the last saved path.
                // In order to avoid matching to that just last saved path when searching,
                // we ignore AlternateContent by default.
                // On the other hand, some of paths in xml are written as relative path from
                // that path.
                // So, save it to the temporary dest file with Office's "SaveAs" method in order
                // to update the last saved path to the temporary dest path.
                // When saving, the paths written as relative paths are updated to absolete path
                // with the original last saved path.
                document.Value.SaveAs(tmpFilePath, format);
            }
            return tmpFilePath;
        }
    }
}
