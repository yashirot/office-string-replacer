﻿using Microsoft.Office.Interop.Access;
using Microsoft.Office.Interop.Access.Dao;
using OfficeStringReplacerCore.Infrastructure;
using OfficeStringReplacerCore.Model;
using OfficeStringReplacerCore.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;

namespace OfficeStringReplacerCore.Domain.Processer
{
    public abstract class ShortcutProcesser : Processer
    {
        public ShortcutProcesser(Context context, ProcesserParam param) : base(context, param)
        {
        }

        public bool IsTarget(string path)
        {
            var ext = Path.GetExtension(path);
            switch (ext)
            {
                case ".lnk":
                    return true;
                default:
                    return false;
            }
        }

        public void Process()
        {
            Context.Status = ContextStatus.Excecuting;
            var fullPath = ProcesserParam.TargetFullPath;
            try
            {
                if (ProcesserParam.ExcludeFilePathPatternList.Any(_ => Regex.IsMatch(fullPath, _, RegexOptions.IgnoreCase)))
                {
                    Context.Status = ContextStatus.Success;
                    return;
                }

                if (Directory.Exists(fullPath))
                {
                    var files = Utils.GetFiles(fullPath, @"\.lnk$");
                    foreach (var file in files)
                    {
                        Process(file);
                        ThrowIfCancelRequested();
                        //Search all files
                        Context.Status = ContextStatus.Excecuting;
                        Context.SubStatus = ContextSubStatus.NotProcessed;
                    }
                }
                else if (File.Exists(fullPath))
                {
                    Process(fullPath);
                }
                if (Context.FailedFiles.Any())
                {
                    Context.Status = ContextStatus.Fail;
                }
                else
                {
                    Context.Status = ContextStatus.Success;
                }
            }
            catch (CancelException ex)
            {
                Context.Status = ContextStatus.Canceled;
                Logger.Info($"Canceled: {ex}");
                throw;
            }
        }

        public void Process(string filePath)
        {
            Context.Status = ContextStatus.Excecuting;
            var fullPath = Path.GetFullPath(filePath);
            var ext = Path.GetExtension(fullPath);
            var tmpFilePath = Utils.CreateUniqTempFilePath() + ext;
            try
            {
                if (!IsTarget(filePath))
                {
                    Context.Status = ContextStatus.Success;
                    return;
                }
                var fileName = Path.GetFileName(filePath);
                if (fileName.StartsWith("~$"))
                {
                    //Temporary file
                    Context.Status = ContextStatus.Success;
                    Context.AddSuccessFiles(filePath);
                    return;
                }
                if (ProcesserParam.ExcludeFilePathPatternList.Any(_ => Regex.IsMatch(filePath, _, RegexOptions.IgnoreCase)))
                {
                    Context.Status = ContextStatus.Success;
                    return;
                }
                Logger.Info($"Start processing:{fullPath}");
                Context.ProcessingFile = fullPath;
                File.Copy(fullPath, tmpFilePath);
                PreProcess(tmpFilePath);

                using (var shell = new DisposableCom<IWshRuntimeLibrary.WshShell>(new IWshRuntimeLibrary.WshShell()))
                using (var shortcut = new DisposableCom<IWshRuntimeLibrary.IWshShortcut>(shell.Value.CreateShortcut(tmpFilePath)))
                {
                    ValueProcesser.Process(Context, shortcut.Value, nameof(shortcut.Value.TargetPath));
                    if (Context.SkipRest)
                    {
                        return;
                    }
                    ValueProcesser.Process(Context, shortcut.Value, nameof(shortcut.Value.IconLocation));
                    if (Context.SkipRest)
                    {
                        return;
                    }
                    ValueProcesser.Process(Context, shortcut.Value, nameof(shortcut.Value.WorkingDirectory));
                    if (Context.SkipRest)
                    {
                        return;
                    }
                    ValueProcesser.Process(Context, shortcut.Value, nameof(shortcut.Value.Description));
                    if (Context.SkipRest)
                    {
                        return;
                    }
                    shortcut.Value.Save();
                }

                PostProcess(tmpFilePath, fullPath);
                Context.Status = ContextStatus.Success;
            }
            catch (CancelException ex)
            {
                Context.Status = ContextStatus.Canceled;
                Logger.Info($"Canceled: {ex}");
                throw;
            }
            catch (Exception ex)
            {
                Context.Status = ContextStatus.Fail;
                Logger.Error($"{ex}");
            }
            finally
            {
                switch (Context.Status)
                {
                    case ContextStatus.Success:
                        Context.AddSuccessFiles(fullPath);
                        switch (Context.SubStatus)
                        {
                            case ContextSubStatus.Processed:
                                Context.AddProcessedFiles(fullPath);
                                break;
                        }
                        break;
                    case ContextStatus.Fail:
                        Context.AddFailedFiles(fullPath);
                        break;
                }
                if (!string.IsNullOrEmpty(tmpFilePath) && File.Exists(tmpFilePath))
                {
                    File.Delete(tmpFilePath);
                }
                Logger.Info($"Finish processing:{fullPath}");
            }
        }

        protected virtual void PreProcess(string sourceFolderPath)
        {
        }

        protected virtual void PostProcess(string sourceFolderPath, string destFilePath)
        {
        }
    }
}
