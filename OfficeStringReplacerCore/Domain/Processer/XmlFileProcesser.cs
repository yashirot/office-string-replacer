﻿using OfficeStringReplacerCore.Infrastructure;
using OfficeStringReplacerCore.Model;
using System.IO;
using System.Xml;

namespace OfficeStringReplacerCore.Domain.Processer
{
    public class XmlFileProcesser : Processer
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="targetString">Search Target</param>
        public XmlFileProcesser(Context context, ProcesserParam param) : base(context, param)
        {
        }

        public void Process(string filePath)
        {
            var fullPath = Path.GetFullPath(filePath);
            Logger.Debug($"Start processing: {fullPath}");
            PreProcess(fullPath);
            var xmlDocument = new XmlDocument();
            xmlDocument.Load(Path.GetFullPath(fullPath));
            Process(xmlDocument);
            if (Context.SkipRest)
            {
                return;
            }
            PostProcess(xmlDocument, fullPath);
            Logger.Debug($"Finish processing: {filePath}");
        }

        private void Replace(XmlNodeList nodeList)
        {
            foreach (XmlNode childNode in nodeList)
            {
                Process(childNode);
            }
        }

        private void Process(XmlNode node)
        {
            if (ProcesserParam.XmlTagsToBeIgnored.Contains(node.Name))
            {
                return;
            }
            if (node.NodeType == XmlNodeType.XmlDeclaration)
            {
                return;
            }
            if (node.Attributes != null)
            {
                foreach (XmlAttribute attibute in node.Attributes)
                {
                    ValueProcesser.Process(Context, attibute, nameof(attibute.Value));
                    if (Context.SkipRest)
                    {
                        return;
                    }
                }
            }
            if (node.HasChildNodes)
            {
                Replace(node.ChildNodes);
            }
            else
            {
                if (!string.IsNullOrEmpty(node.Value))
                {
                    ValueProcesser.Process(Context, node, nameof(node.Value));
                    if (Context.SkipRest)
                    {
                        return;
                    }
                }
            }
        }

        protected virtual void PreProcess(string sourceFilePath)
        {
        }

        protected virtual void PostProcess(XmlDocument document, string destFilePath)
        {
        }
    }
}
