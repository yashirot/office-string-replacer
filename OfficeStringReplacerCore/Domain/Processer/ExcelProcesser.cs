﻿using Microsoft.Office.Interop.Excel;
using Microsoft.Vbe.Interop;
using OfficeStringReplacerCore.Model;
using OfficeStringReplacerCore.Util;
using System;
using System.IO;

namespace OfficeStringReplacerCore.Domain.Processer
{
    public abstract class ExcelProcesser : XmlOfficeProcesser
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="from">Replace from</param>
        /// <param name="to">Replace to</param>
        public ExcelProcesser(Context context, ProcesserParam param) : base(context, param)
        {
        }

        public override bool IsTarget(string path)
        {
            var ext = Path.GetExtension(path);
            switch (ext)
            {
                case ".xlsx":
                case ".xlsm":
                case ".xls":
                    return true;
                default:
                    return false;
            }
        }

        protected override bool IsMacroEnabled(string path)
        {
            var ext = Path.GetExtension(path);
            switch (ext)
            {
                case ".xls":
                    using (var excelApp = new DisposableCom<Microsoft.Office.Interop.Excel.Application>(
                        new Microsoft.Office.Interop.Excel.Application()
                        {
                            EnableEvents = false
                        },
                        (_) => { _.Quit(); }))
                    using (var workbooks = new DisposableCom<Workbooks>(excelApp.Value.Workbooks))
                    using (var workbook = new DisposableCom<Workbook>(
                        workbooks.Value.Open(path, IgnoreReadOnlyRecommended: true),
                        (_) => { _.Close(false); }))
                    {
                        return workbook.Value.HasVBProject;
                    }
                case ".xlsm":
                    return true;
                default:
                    return false;
            }
        }

        protected override string CopyAsTempOpenXMLFile(string sourceFilePath, bool isMacroEnabled)
        {
            var format = isMacroEnabled ?
                XlFileFormat.xlOpenXMLWorkbookMacroEnabled :
                XlFileFormat.xlOpenXMLWorkbook;
            var tmpFilePath = isMacroEnabled ?
                Utils.CreateUniqTempFilePath() + ".xlsm" :
                Utils.CreateUniqTempFilePath() + ".xlsx";
            using (var excelApp = new DisposableCom<Microsoft.Office.Interop.Excel.Application>(
                new Microsoft.Office.Interop.Excel.Application()
                {
                    EnableEvents = false
                },
                (_) => { _.Quit(); }))
            using (var workbooks = new DisposableCom<Workbooks>(excelApp.Value.Workbooks))
            using (var workbook = new DisposableCom<Workbook>(
                workbooks.Value.Open(sourceFilePath, IgnoreReadOnlyRecommended: true),
                (_) => { _.Close(false); }))
            {
                workbook.Value.SaveAs(tmpFilePath, format);
            }
            return tmpFilePath;
        }
    }
}
