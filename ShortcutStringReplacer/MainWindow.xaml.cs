﻿using OfficeStringReplacerCore.Definision;
using OfficeStringReplacerCore.Domain.Replacer;
using OfficeStringReplacerCore.Domain.Searcher;
using OfficeStringReplacerCore.Infrastructure;
using OfficeStringReplacerCore.Model;
using OfficeStringReplacerCore.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using NLog;
using System.Reflection;

namespace ShortcutStringReplacer
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly Logger Logger = LogManager.GetLogger(nameof(MainWindow));

        private Config AppConfig { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            Environments.AppName = "ShortcutStringReplacer";
            Directory.CreateDirectory(Utils.GetUserDataDir());
            var config = ConfigLoader.Load();
            AppConfig = config;
            fromTextBox.Text = config.From;
            toTextBox.Text = config.To;
            caseCheckBox.IsChecked = config.IgnoreCase;
            var asm = Assembly.GetExecutingAssembly();
            var version = asm.GetName().Version.ToString();
            Logger.Info($"Start {Environments.AppName} {version}");
            Logger.Info($"Core version: {Utils.GetCoreVersion()}");
            pathTextBox.AddHandler(DragOverEvent, new System.Windows.DragEventHandler(OnDragOverTextBox), true);
            pathTextBox.AddHandler(DropEvent, new System.Windows.DragEventHandler(OnDropPathTextBox), true);
            backupTextBox.AddHandler(DragOverEvent, new System.Windows.DragEventHandler(OnDragOverTextBox), true);
            backupTextBox.AddHandler(DropEvent, new System.Windows.DragEventHandler(OnDropBackupTextBox), true);
        }

        private Context ExecutingTaskContext { get; set; }

        private void ChangeButtonsAvailability(bool enabled)
        {
            Dispatcher.Invoke(() =>
            {
                searchButton.IsEnabled = enabled;
                saveListButton.IsEnabled = enabled;
                if (Environments.EvaluationMode)
                {
                    IsEnabled = false;
                }
                else
                {
                    replaceButton.IsEnabled = enabled;
                }
            });
        }

        private void UpdateStatusBarLabel(string message)
        {
            Dispatcher.Invoke(() =>
            {
                statusBarLabel.Content = message;
            });
        }
        private void SelectFolder(System.Windows.Controls.TextBox textBox)
        {
            using (var folderBrowserDialog = new FolderBrowserDialog()
            {
                Description = "Select folder",
            })
            {
                if (folderBrowserDialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }

                textBox.Text = folderBrowserDialog.SelectedPath;
            }
        }

        private void OnClickSearchPathSelectFolderButton(object sender, RoutedEventArgs e)
        {
            SelectFolder(pathTextBox);
        }

        private void OnClickBackupSelectFolderButton(object sender, RoutedEventArgs e)
        {
            SelectFolder(backupTextBox);
        }

        private void OnClickSearchButton(object sender, RoutedEventArgs e)
        {
            if (ExecutingTaskContext != null)
            {
                return;
            }
            targetListBox.Items.Clear();
            var path = pathTextBox.Text;
            var from = fromTextBox.Text;
            var to = toTextBox.Text;
            var ignoreCase = caseCheckBox.IsChecked ?? false;

            Logger.Info($"Path: {path}, From: {from}, To: {to}, IgnoreCase: {ignoreCase}");

            Task.Run(() =>
            {
                var logFilePath = Utils.GetCurrentLogFilePath();
                ChangeButtonsAvailability(false);
                try
                {
                    var searchFailedFilesPath = Utils.GetSearchFailedPath();
                    if (string.IsNullOrWhiteSpace(path))
                    {
                        return;
                    }
                    var param = new ProcesserParam()
                    {
                        TargetPath = path,
                        ExcludeFilePathPatternList = new List<string>
                            {
                                $".*{Consts.BackupFolderBase}.*"
                            },
                        ValueProcesserParam = new ValueProcesserParam()
                        {
                            From = from,
                            To = to,
                            IgnoreCase = ignoreCase,
                        },
                        XmlTagsToBeIgnored = AppConfig.XmlTagsToBeIgnored
                    };
                    var context = new Context();
                    ExecutingTaskContext = context;

                    bool continueUpdateLabel = true;
                    var task = Task.Run(() =>
                    {
                        while (continueUpdateLabel)
                        {
                            Task.Delay(500).Wait();
                            UpdateStatusBarLabel($"処理中: {context.ProcessingFile}");
                        }
                    });

                    try
                    {
                        var shortcutSearcher = new ShortcutSearcher(context, param);
                        shortcutSearcher.Process();
                    }
                    finally
                    {
                        continueUpdateLabel = false;
                        task.Wait();
                        UpdateStatusBarLabel("");
                    }

                    var proccessedFiles = context.ProcessedFiles;
                    var failedFiles = context.FailedFiles;
                    if (failedFiles.Any())
                    {
                        File.WriteAllLines(searchFailedFilesPath, failedFiles);
                        var message = $"検索に失敗したファイルが存在します。\n\n詳細は\nファイルリスト（{searchFailedFilesPath}）\nログ（{logFilePath}）\nを確認してください。";
                        System.Windows.MessageBox.Show(message, "検索");
                    }

                    Dispatcher.Invoke(() =>
                    {
                        foreach (var file in proccessedFiles)
                        {
                            targetListBox.Items.Add(file);
                        }
                        targetListBox.SelectAll();
                    });
                    if (!proccessedFiles.Any())
                    {
                        System.Windows.MessageBox.Show("ファイルが見つかりませんでした", "検索");
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("完了しました。", "検索");
                    }
                }
                catch (CancelException ex)
                {
                    Logger.Info(ex);
                    var message = $"キャンセルされました";
                    System.Windows.MessageBox.Show(message, "検索");
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                    var message = $"検索処理中にエラーが発生しました。\n\n詳細はログファイル（{logFilePath}）を確認してください。";
                    System.Windows.MessageBox.Show(message, "検索");
                }
                finally
                {
                    ExecutingTaskContext = null;
                    ChangeButtonsAvailability(true);
                }
            });
        }

        private void OnClickSaveListButton(object sender, RoutedEventArgs e)
        {
            var logFilePath = Utils.GetCurrentLogFilePath();
            try
            {
                var items = targetListBox.Items;
                if (items is null)
                {
                    return;
                }
                using (var saveFileDialog = new SaveFileDialog()
                {
                    FileName = "SearchResult.csv",
                    Filter = "CSV File(*.csv)|*.csv|Text File(*.txt)|*.txt"
                })
                {
                    if (saveFileDialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                    {
                        return;
                    }
                    var fileName = saveFileDialog.FileName;
                    using (var writer = new StreamWriter(fileName, false, Encoding.UTF8))
                    {
                        var itemHash = new HashSet<string>();
                        foreach (var item in items)
                        {
                            itemHash.Add(item.ToString());
                        }
                        var ext = Path.GetExtension(fileName).ToLowerInvariant();
                        string outputString = "";
                        switch (ext)
                        {
                            case ".csv":
                                var escapedItemHash = itemHash
                                    .Select(_ => _.Replace("\"", "\"\""))
                                    .Select(_ => $"\"{_}\"");
                                outputString = string.Join(",", escapedItemHash);
                                break;
                            default:
                                outputString = string.Join("\n", itemHash);
                                break;
                        }
                        writer.WriteLine(outputString);
                    }
                }
                System.Windows.MessageBox.Show("完了しました。", "保存");
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                var message = $"リスト保存中にエラーが発生しました。\n\n詳細はログファイル（{logFilePath}）を確認してください。";
                System.Windows.MessageBox.Show(message, "保存");
            }
        }

        private void OnClickReplaceButton(object sender, RoutedEventArgs e)
        {
            if (ExecutingTaskContext != null)
            {
                return;
            }
            var selectedItem = targetListBox.SelectedItems;
            var from = fromTextBox.Text;
            var to = toTextBox.Text;
            var ignoreCase = caseCheckBox.IsChecked ?? false;
            var backupDirectory = backupTextBox.Text;

            if (string.IsNullOrWhiteSpace(backupDirectory))
            {
                var message = $"バックアップフォルダーを指定してください。";
                System.Windows.MessageBox.Show(message, "置換");
                return;
            }

            Logger.Info($"From: {from}, To: {to}, IgnoreCase: {ignoreCase}");

            Task.Run(() =>
            {
                var logFilePath = Utils.GetCurrentLogFilePath();
                var replaceFailedFilesPath = Utils.GetReplaceFailedPath();
                ChangeButtonsAvailability(false);
                try
                {
                    var replacedItemList = new List<object>();
                    var backupFileCreater = new FileBackupper(backupDirectory, true);
                    foreach (string item in selectedItem)
                    {
                        var fullPath = Path.GetFullPath(item);
                        UpdateStatusBarLabel($"処理中: {fullPath}");
                        try
                        {
                            backupFileCreater.Backup(fullPath);
                            var param = new ProcesserParam()
                            {
                                TargetPath = fullPath,
                                ValueProcesserParam = new ValueProcesserParam()
                                {
                                    From = from,
                                    To = to,
                                    IgnoreCase = ignoreCase
                                },
                                XmlTagsToBeIgnored = AppConfig.XmlTagsToBeIgnored
                            };
                            var context = new Context();
                            ExecutingTaskContext = context;
                            var shortcutReplacer = new ShortcutReplacer(context, param);
                            shortcutReplacer.Process();
                            if (context.Status == ContextStatus.Fail)
                            {
                                File.AppendAllLines(replaceFailedFilesPath, new[] { fullPath });
                                var message = $"{fullPath}の置換中にエラーが発生しました。\n\n詳細はログファイル（{logFilePath}）を確認してください。";
                                var result = System.Windows.MessageBox.Show(message, "置換", MessageBoxButton.OKCancel);
                                if (result == MessageBoxResult.Cancel)
                                {
                                    break;
                                }
                            }
                            else
                            {
                                replacedItemList.Add(fullPath);
                            }
                            if (context.IsCancelRequested)
                            {
                                throw new CancelException();
                            }
                        }
                        catch (CancelException ex)
                        {
                            throw ex;
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex);
                            File.AppendAllLines(replaceFailedFilesPath, new[] { fullPath });
                            var message = $"{fullPath}の置換中にエラーが発生しました。\n\n詳細はログファイル（{logFilePath}）を確認してください。";
                            var result = System.Windows.MessageBox.Show(message, "置換", MessageBoxButton.OKCancel);
                            if (result == MessageBoxResult.Cancel)
                            {
                                break;
                            }
                        }
                    }
                    Dispatcher.Invoke(() =>
                    {
                        foreach (var replacedItem in replacedItemList)
                        {
                            targetListBox.Items.Remove(replacedItem);
                        }
                    });
                    System.Windows.MessageBox.Show("完了しました。", "置換");
                }
                catch (CancelException ex)
                {
                    Logger.Info(ex);
                    var message = $"キャンセルされました";
                    System.Windows.MessageBox.Show(message, "置換");
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                    var message = $"置換処理中にエラーが発生しました。\n\n詳細はログファイル（{logFilePath}）を確認してください。";
                    System.Windows.MessageBox.Show(message, "置換");
                }
                finally
                {
                    UpdateStatusBarLabel("");
                    ExecutingTaskContext = null;
                    ChangeButtonsAvailability(true);
                }
            });
        }
        private void OnClickCancelButton(object sender, RoutedEventArgs e)
        {
            if (ExecutingTaskContext == null)
            {
                return;
            }
            ExecutingTaskContext.IsCancelRequested = true;
        }

        private void OnDragOverTextBox(object sender, System.Windows.DragEventArgs e)
        {
            if (e.Data.GetDataPresent(System.Windows.DataFormats.FileDrop))
            {
                e.Effects = System.Windows.DragDropEffects.All;
            }
            else
            {
                e.Effects = System.Windows.DragDropEffects.None;
            }
            e.Handled = false;
        }

        private void DropToTextBox(System.Windows.Controls.TextBox textBox, System.Windows.DragEventArgs e)
        {
            var logFilePath = Utils.GetCurrentLogFilePath();
            try
            {
                var dropTargets = e.Data.GetData(System.Windows.DataFormats.FileDrop) as string[];
                if (dropTargets == null)
                {
                    return;
                }
                var dropTarget = dropTargets.FirstOrDefault();
                if (Directory.Exists(dropTarget))
                {
                    textBox.Text = dropTarget;
                }
                if (File.Exists(dropTarget))
                {
                    textBox.Text = dropTarget;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                var message = $"ドロップ処理中にエラーが発生しました。\n\n詳細はログファイル（{logFilePath}）を確認してください。";
                System.Windows.MessageBox.Show(message, "ドロップ");
            }
        }

        private void OnDropPathTextBox(object sender, System.Windows.DragEventArgs e)
        {
            DropToTextBox(pathTextBox, e);
        }

        private void OnDropBackupTextBox(object sender, System.Windows.DragEventArgs e)
        {
            DropToTextBox(backupTextBox, e);
        }
    }
}
