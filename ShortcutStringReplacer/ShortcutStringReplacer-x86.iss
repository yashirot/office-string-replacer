﻿#define MyAppName "ShortcutStringReplacer"
#define AppVersion GetFileVersion(SourcePath + "bin\x86\Release\ShortcutStringReplacer.exe")
[Setup]
AppName={#MyAppName}
AppVerName={#MyAppName}
VersionInfoVersion={#AppVersion}
AppVersion={#AppVersion}
AppPublisher=ClearCode Inc.
DefaultDirName={autopf}\ShortcutStringReplacer
ShowLanguageDialog=no
Compression=lzma2
SolidCompression=yes
OutputDir=dest
OutputBaseFilename=ShortcutStringReplacerSetup-{#SetupSetting("AppVersion")}-x86
VersionInfoDescription=ShortcutStringReplacerSetup
ArchitecturesAllowed=x64
ArchitecturesInstallIn64BitMode=x64
LicenseFile=..\LICENSE.txt
PrivilegesRequired=lowest
PrivilegesRequiredOverridesAllowed=dialog

[Languages]
Name: en; MessagesFile: "compiler:Default.isl"
Name: ja; MessagesFile: "compiler:Languages\Japanese.isl"

[Files]
Source: "bin\x86\Release\*.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "bin\x86\Release\*.config"; DestDir: "{app}"; Flags: ignoreversion
Source: "bin\x86\Release\*.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Licenses\*"; DestDir: "{app}\Licenses"; Flags: ignoreversion recursesubdirs
Source: "..\LICENSE.txt"; DestDir: "{app}"; Flags: ignoreversion
Source: "{src}\DefaultConfig\config.json"; DestDir: "{autoappdata}\{#MyAppName}"; Flags: ignoreversion external skipifsourcedoesntexist

[Dirs]
Name: "{autoappdata}\{#MyAppName}"

[Icons]
Name: "{autodesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppName}.exe"; WorkingDir: "{app}" 