---
CJKmainfont: Noto Sans CJK JP
CJKoptions:
  - BoldFont=Noto Sans CJK JP Bold
#titlepage-logo: ./media/favicon.png
title: |
  OfficeStringReplacer  
  利用マニュアル
subject: OfficeStringReplacer利用マニュアル
date: 2024/01/xx
author: 株式会社クリアコード
keywords: [OfficeStringReplacer, Users guide]
titlepage: true
toc-title: 目次
toc-own-page: true
---

更新履歴

| 日付       | Version    | 備考                             |
|------------|------------|----------------------------------|
| 2024/01/xx | 1.0.0.13   | 新規作成                         |
|            |            |                                  |

**本書について**

本書は、2024年1月時点のデータにより作成されており、それ以降の状況の変動によっては、本書の内容と事実が異なる場合があります。また、本書の内容に基づく運用結果については責任を負いかねますので、あらかじめご了承ください。

本書で使用するシステム名、製品名は、各社の商標または登録商標です。本文中ではTM、®、©マークは省略しています。

\newpage
# OfficeStringReplacerについて

OfficeStringReplacerは、ファイル内の文字列を検索・置換するツールです。

Microsoft Office製品で使用する各種ファイルを検索・置換できます。

## サポート対象のファイル形式
次のファイル形式をサポートしています。

* Excel
  * `.xlsx`
  * `.xlsm`
  * `.xls`
* Word
  * `.docx`
  * `.docm`
  * `.doc`
* PowerPoint
  * `.pptx`
  * `.pptm`
  * `.ppt`
* Access
  * `.accdb`
  * `.mdb`
* ショートカット
  * `.lnk`

## 制限事項
Microsoft Office製品で使用するファイルのうち、次はサポート対象外です。

* パスワードロックされたもの
* 書き込み権限がないもの
* Accessマクロ　■VBAマクロはサポート対象です

\newpage
# 使い方

## 動作環境

* Windows 11 または Windows 10
* Microsoft Office がインストールされていること
* Excel、Word、PowerPointにて、VBAプロジェクト オブジェクト モデルへのアクセスを信頼していること

### VBAプロジェクト オブジェクト モデルへのアクセス許可
Excel、Word、PowerPointでは、あらかじめ「VBAプロジェクト オブジェクト モデルへのアクセスを信頼する」を有効にする必要があります。

1. 各アプリの「オプション」 -> 「トラストセンター」 -> 「トラストセンターの設定」 -> 「マクロの設定」 へ進みます
2. 「開発者向けのマクロ設定」の「VBAプロジェクト オブジェクト モデルへのアクセスを信頼する」にチェックを入れ[OK]ボタンを押下します

Excel  
  ![](media/2-1_trustVBA-Excel.png)

Word  
  ![](media/2-1_trustVBA-Word.png)

PowerPoint  
  ![](media/2-1_trustVBA-PowerPoint.png)

## 検索・置換のための設定ファイル
JSON形式の設定ファイル`config.json`により、検索・置換のために必要な情報を指定します。
初期状態では次の内容になっています。

```
{
    "From": null,
    "To": null,
    "IgnoreCase": false,
    "XmlTagsToBeIgnored": [
        "mc:AlternateContent"
    ],
    "BackupDirectory": null
}
```
キーごとに次の値を指定します。

* `From`
  * 起動時のデフォルトの「置換前文字列」を指定します。
  * デフォルト値: `null`
  * 例: `"From" : "foo"`
* `To`
  * 起動時のデフォルトの「置換後文字列」を指定します。
  * デフォルト値: `null`
  * 例: `"To" : "bar"`
* `IgnoreCase`
  * 起動時のデフォルトの「大文字小文字を無視する」を指定します。
  * デフォルト値: `false`
  * 例: `"IgnoreCase" : true`
* `XmlTagsToBeIgnored`
  * OfficeファイルをOpen XMLとして展開したとき、検索、置換から無視するXMLのタグ名をリスト形式で指定します。
  * デフォルト値: 
    ```    
    "XmlTagsToBeIgnored": [
        "mc:AlternateContent"
    ]
    ```
  * 例: 
    ```
    "XmlTagsToBeIgnored": [
        "mc:AlternateContent",
        "sheets"
    ]
    ```
  * たとえば上のように指定した場合、`<sheets>`タグ（要素）配下は検索と置換時に無視されます。このときOfficeファイル内に下記のようなXMLが存在しても、「Sheet1」や「Sheet2」の検索結果は対象外となります。
    ```
    <sheets>
      <sheet name="Sheet1" sheetId="1" r:id="rId1"/>
      <sheet name="Sheet2" sheetId="2" r:id="rId2"/>
    </sheets>
    ```
* `BackupDirectory`
  * 起動時のデフォルトの「バックアップフォルダー」を指定します。
  * デフォルト値: `null`
  * 例: `"BackupDirectory" : "D:\\"`

* 文字列には""が必要です。
* パスの`\`は、エスケープ文字を加えて`\\`と指定します。

## インストールと起動

任意のフォルダーに`DefaultConfig\config.json`とセットアップ用のexeファイルを配置します。

（DefaultConfigをデスクトップに配置した例）

![](media/2-3_DefaultConfig.png)

64bitのOfficeを使用している場合、`OfficeStringReplacerSetup-{version}-x64-Office.exe`を実行します。

![](media/2-3_Setup-x64.png)

32bitのOfficeを使用している場合、`OfficeStringReplacerSetup-{version}-x86-Office.exe`を実行します。

![](media/2-3_Setup-x86.png)

Windowsのアーキテクチャではなく、Officeのアーキテクチャであることに注意してください。

インストールが完了すると、デスクトップに起動用のアイコンが配置されます。

![](media/2-3_Launching-Icon.png)

設定ファイルは`%APPDATA%\OfficeStringReplacer\config.json`に配置されます。

また、検索と置換に関するログ情報も`%APPDATA%\OfficeStringReplacer\`配下に記録されます。

起動用アイコンをダブルクリックすると、メイン画面が表示されます。

![](media/2-3_Screen-Configured.png)

`config.json`の指定値が反映されています。

`DefaultConfig\config.json`を配置せずにインストールした場合は、既定値となります。

## 検索と置換

検索と置換の手順は次のとおりです。

1. 「Office文字列置換」画面上で次の項目を入力・指定します。

![](media/2-4_Step1-Screen.png)

* 「検索対象パス」　検索対象のフォルダーまたはファイルを入力します
* 「バックアップフォルダー」　置換時にファイルをバックアップするフォルダーを指定します
  * 指定したフォルダー配下に`osr.backup.{変換日時}`フォルダーを作成し、さらにその配下に元のファイルがバックアップされます
  * ルートパス（ドライブやファイルサーバ名）からのディレクトリ構造ごとバックアップされます
    * 例えば、バックアップフォルダーが`D:\backup`で、置換対象のファイルが`C:\testfolder\testfile.lnk`の場合、`D:\backup\osr.backup.{変換日時}\testfolder\testfile.lnk`にバックアップされます
* 「対象ファイル」　対象となるファイルの形式を選択します
* 「置換前文字列」　置換前の文字列を指定します
* 「置換後文字列」　置換後の文字列を指定します
* 「大文字小文字を区別しない」 置換前文字列の大文字小文字を同一視する場合にチェックします

2. [対象ファイルを検索] ボタンを押下します。

* フォルダーは再帰的に検索されます
* 「対象ファイル」セレクトボックスに、「置換前文字列」を含むファイル一覧が出力されます

![](media/2-4_Step2-Screen.png)

* 検索を中止する場合は、処理中に[キャンセル]を押下します
* また、[リストを保存] ボタンを押下すると、対象ファイルのリストをファイルへ出力できます。保存する際、ファイルの種類で「csv」を選ぶとカンマ区切り形式に、「txt」を選ぶと改行区切りとなります。

3. 「置換後文字列」に変換するファイルを選択します。

* 選択したファイルは背景に色が付きます

![](media/2-4_Step3-Screen.png)

4. [選択したファイルを置換]ボタンを押下すると、選択した対象ファイル内の文字列を置換します。

* 「置換前文字列」が「置換後文字列」に変換されます

![](media/2-4_Step4-Replaced.png)

* 文字列が置換されたファイルは「対象ファイル」から外れます

## 終了とアンインストール

OfficeStringReplacerを終了するにはウィンドウ右上の[x] （閉じる）ボタンを押下します。

![](media/2-5_Close.png)

アンインストールはコントロールパネルの「プログラムと機能」から実行します。

![](media/2-5_Uninstall.png)

\newpage
# FAQ

Q. 検索の処理が止まってしまい、反応がありません。

A. 対象ファイルの検索時に、応答を要求するメッセージが表示されていないか確認してください（ウィンドウ位置によっては背後に隠れている場合もあります）。

たとえばWord差し込み印刷のメイン文書は、データファイルとリンクしているため次のように表示されることがあります。

（表示例）

![](media/3-Message-on-Mailings.png)

> この文書を開くと、次の SQL コマンドが実行されます。  
> SELECT * FROM （データソース）  
> データベースからのデータが、文書に挿入されます。続行しますか? 

この場合、「はい」または「いいえ」を選択して進めてください。どちらを選んでも検索・置換結果は同じです。

そのほか、操作に関して不明な点がありましたら、サポート窓口まで問い合わせください。